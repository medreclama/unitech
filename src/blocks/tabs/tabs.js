import Swiper, { Navigation } from 'swiper';

Swiper.use([Navigation]);

export default function tabs() {
  const tabsList = Array.from(document.querySelectorAll('.tabs'));
  tabsList.forEach((block) => {
    const tl = Array.from(block.querySelectorAll('.tabs__label'));
    const tc = Array.from(block.querySelectorAll('.tabs__item'));
    tl.forEach((label, i) => {
      const tab = tc[i];
      if (+i === 0) {
        label.classList.add('tabs__label--active');
        tab.classList.add('tabs__item--active');
      }
      label.addEventListener('click', () => {
        if (!tc[i].classList.contains('tabs__item--active')) {
          tl.forEach((item, j) => {
            tc[j].classList.remove('tabs__item--active');
            tl[j].classList.remove('tabs__label--active');
            return item;
          });
          tab.classList.add('tabs__item--active');
          label.classList.add('tabs__label--active');
        }
      });
      return label;
    });
    const swiperLabels = block.querySelector('.tabs__labels--swipe');
    if (swiperLabels) {
      let swiper;
      const swiperInit = () => {
        // eslint-disable-next-line
        swiper = new Swiper(swiperLabels, {
          navigation: {
            nextEl: '.tabs__change--next',
            prevEl: '.tabs__change--prev',
          },
          slidesPerView: 'auto',
        });
      };
      if (window.innerWidth < 1000) swiperInit();
      window.addEventListener('resize', () => {
        requestAnimationFrame(() => {
          if (window.innerWidth < 1000 && !(swiper instanceof Swiper)) swiperInit();
          if (window.innerWidth >= 1000 && swiper instanceof Swiper) {
            swiper.destroy();
            swiper = null;
          }
        });
      });
    }
    return block;
  });
}
