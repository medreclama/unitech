import media from '../match-media/match-media';

const header = () => {
  const html = document.querySelector('html');
  const switcher = document.querySelector('.header__switcher');
  const close = document.querySelector('.header__close');
  const menu = document.querySelector('.header__menu');
  switcher.addEventListener('click', () => {
    setTimeout(() => menu.classList.add('header__menu--active'), 1000 / 60);
    const addMediaMenu = () => {
      media.addChangeListener('min', 'l', addMediaMenu);
      if (media.min('l')) {
        html.style.overflow = 'auto';
        menu.style.display = 'flex';
      } else {
        menu.style.display = 'grid';
        html.style.overflow = 'hidden';
      }
    };
    addMediaMenu();
  });
  close.addEventListener('click', () => {
    menu.classList.remove('header__menu--active');
    html.style.overflow = 'auto';
    setTimeout(() => { menu.style.display = 'none'; }, 400);
  });
};

export default header;
