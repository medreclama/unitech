const counter = () => {
  const counters = document.querySelectorAll('.counter');
  const buttonState = (button, value) => {
    if (value < 2) {
      button.setAttribute('disabled', 'disabled');
      button.classList.add('counter__change--disabled');
    } else if (button.classList.contains('counter__change--disabled') && button.hasAttribute('disabled')) {
      button.removeAttribute('disabled');
      button.classList.remove('counter__change--disabled');
    }
  };
  if (!counter) return;
  Array.from(counters).forEach((c) => {
    const input = c.querySelector('.counter__input');
    const dec = c.querySelector('.counter__change--decrease');
    buttonState(dec, +input.value);
    c.addEventListener('click', (event) => {
      let val = +input.value;
      const { target } = event;
      if (!target.classList.contains('counter__change')) return;
      if (target.dataset.change === 'dec') {
        val -= 1;
        buttonState(dec, val);
      }
      if (target.dataset.change === 'inc') {
        val += 1;
        buttonState(dec, val);
      }
      input.value = val;
    });
  });
};

export default counter;
