const mrcSH = () => {
  const mrcSHTitles = Array.from(document.querySelectorAll('[data-mrc-sh-title]'));
  const mrcSHBodies = Array.from(document.querySelectorAll('[data-mrc-sh-body]'));
  mrcSHTitles.forEach((title) => {
    const itemObj = { name: title, bodies: [] };
    const itemName = title.dataset.mrcShTitle;
    const bodiesList = [];
    mrcSHBodies.forEach((body) => {
      const itemBody = body;
      const bodyName = itemBody.dataset.mrcShBody;
      if (itemName === bodyName) {
        bodiesList.push(itemBody);
        itemBody.style.display = 'none';
      }
    });
    if (bodiesList) {
      itemObj.bodies = bodiesList;
      itemObj.name.addEventListener('click', (e) => {
        e.preventDefault();
        bodiesList.forEach((item) => {
          const currentItem = item;
          currentItem.style.display = currentItem.style.display === 'none' ? '' : 'none';
        });
      });
    }
  });
};

export default mrcSH;
