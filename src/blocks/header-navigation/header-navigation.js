const headerNavigation = () => {
  const parents = document.querySelectorAll('.header-navigation__item--parent');
  if (!parents) return;
  const parentsArray = Array.from(parents).filter((parent) => !parent.closest('.header-navigation__item--catalog'));
  parentsArray.forEach((parent) => {
    const link = parent.firstElementChild;
    const opener = '<div class="header-navigation__opener"></div>';
    link.insertAdjacentHTML('afterend', opener);
    parent.addEventListener('click', (event) => {
      if (event.target.classList.contains('header-navigation__opener')) event.currentTarget.classList.toggle('header-navigation__item--active');
    });
  });
};

export default headerNavigation;
