import lightbox from './modules/lightbox';
import sh from '../blocks/show-hide/show-hide';
import tabs from '../blocks/tabs/tabs';
import header from '../blocks/header/header';
import headerNavigation from '../blocks/header-navigation/header-navigation';
import Form from '../blocks/form/form';
import modal from '../blocks/modal/modal';
import accordionItem from '../blocks/accordion-item/accordion-item';

lightbox();
sh();
tabs();
header();
headerNavigation();
modal();

const formsCreate = () => {
  const validation = {
    methods: {
      tel: {
        rules: [
          (i) => {
            const message = 'Введите правильный номер телефона без восьмёрки';
            return i.value.length !== '+7 (000) 000-00-00'.length ? message : '';
          },
        ],
        type: 'type',
      },
      email: {
        rules: [
          (i) => {
            const message = 'Введите правильный e-mail-адрес';
            return !i.value.toLowerCase().match(/^[a-z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-z0-9.-]+$/) ? message : '';
          },
        ],
        type: 'type',
      },
      name: {
        rules: [
          (i) => {
            const message = 'Минимум три символа';
            return i.value.length > 2 ? '' : message;
          },
          (i) => {
            const message = 'Только русские имена';
            return i.value.match(/[аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯ]+/ig) ? '' : message;
          },
        ],
        type: 'name',
      },
      agreement: {
        type: 'name',
        required: 'Согласитесь на обработку персональных данных',
      },
    },
  };

  const homeHeaderForm = document.querySelectorAll('.home-header__form');
  const callBackForm = document.querySelector('.call-back__form');
  const calculateForm = document.querySelector('.calculate-form');
  const calculateFormModal = document.querySelector('.modal-calculate__form');
  const quickBuyFormModal = document.querySelector('.modal-quick-buy__form');
  const addCommentModal = document.querySelector('#add_comment');

  const message = {
    success: {
      title: 'Спасибо за заявку',
      text: 'Наши менеджеры свяжутся с вами в ближайшее время',
    },
    error: {
      title: 'Произошла ошибка',
      text: '',
    },
  };

  const calculateMessage = {
    success: {
      title: 'Результат',
      text: '',
    },
    error: {
      title: 'Произошла ошибка',
      text: '',
    },
  };

  const quickBuyMessage = {
    success: { title: 'Заказ оформлен', text: 'Перезвоним в течении часа рабочего времени.<br>Ежедневно с 9:00 до 18:00' },
    error: { title: 'Произошла ошибка', text: '' },
  };

  const addCommentMessage = {
    success: { title: 'Спасибо!', text: 'После модерации комментарий будет опубликован' },
    error: { title: 'Произошла ошибка', text: '' },
  };

  if (homeHeaderForm) {
    Array.from(homeHeaderForm).forEach((form) => {
      new Form({
        form,
        message,
      }, validation).init();
    });
  }

  if (callBackForm) {
    new Form({
      form: callBackForm,
      message,
      messageModal: false,
    }, validation).init();
  }

  if (calculateForm) {
    new Form({
      form: calculateForm,
      message: calculateMessage,
    }, validation).init();
  }

  if (calculateFormModal) {
    new Form({
      form: calculateFormModal,
      message: calculateMessage,
      messageModal: false,
    }, validation).init();
  }

  if (quickBuyFormModal) {
    validation.methods.tel.required = 'Напишите телефон, мы перезвоним и подтвердим заказ';
    new Form({
      form: quickBuyFormModal,
      message: quickBuyMessage,
      messageModal: false,
    }, validation).init();
  }

  if (addCommentModal) {
    new Form({
      form: addCommentModal,
      message: addCommentMessage,
      messageModal: false,
    }, validation).init();
  }
};
formsCreate();
accordionItem();
